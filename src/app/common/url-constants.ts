/**
 * Created by consultadd on 6/12/16.
 */

import { environment } from '../../environments/environment';
export const HOME = window.location.origin;

let URL = environment.apiUrl;


// let params = new URLSearchParams();
// params.set('city',localStorage.getItem('city') );
let BASEURL = URL + "/api/v1/";
let NEWBASEURL = URL + "/api/v2/";

//Citrus Sandbox's URLS - Wallet Service Integration Testing
let CW_BASE_URL = "https://sandboxadmin.citruspay.com";
let CW_API_ENDPOINT = "/service/um/find_or_create/user/";

// MSG91 OTP SERVICE PROVIDER URL
let OTP_BASE_URL = 'https://sendotp.msg91.com/api/';


export const MEDIAURL = URL;
const AUTHURL = URL + "/api/rest-auth/";
const AUTH_USER_BASE_URL = URL + '/api/users/';

export const URLS = {
  GETCITIES: BASEURL+"citymovies/cities/",
  GETMOVIE: BASEURL+"citymovies/movies/",
  GETMOVIEBYID: BASEURL+"movies/",
  SIGNUP:AUTHURL+"registration/v2/",
  GETTHEATRE : NEWBASEURL + "movies/",

  LOGIN: AUTHURL +"v2/login/",
  AUTH_PROFILE: URL + '/api/users/get_user_object/',
  USER_UPDATE_PASSWORD: BASEURL + 'users/update_password/',
  AUTH_USER_DETAIL: URL + '/api/rest-auth/user/',
  AUTH_USER_LOGOUT: URL + '/api/rest-auth/logout/',
  PASSWORD_CHANGE: URL + '/api/rest-auth/password/change/',
  PASSWORD_RESET: URL + '/api/rest-auth/v2/password/reset/',

  PASSWORD_RESET_CONFIRM: URL + '/api/rest-auth/password/reset/confirm/',



};
