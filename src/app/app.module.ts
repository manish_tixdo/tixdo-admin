import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import {RestService} from "./services/rest.service";
import {EmitterService} from "./services/utils.services";
import {HashLocationStrategy, LocationStrategy} from "@angular/common";
import {ClarityModule} from "clarity-angular/index";
import {routing} from "./app.routing";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ClarityModule,
    routing
  ],
  providers: [RestService, EmitterService,{ provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
