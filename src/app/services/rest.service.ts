import {Injectable} from '@angular/core';
import {Http, Response, RequestOptionsArgs, RequestOptions, Headers} from '@angular/http'
import 'rxjs/Rx';

import {Observable} from "rxjs/Observable";
import {EmitterService} from "./utils.services";
import {contentHeaders} from "../common/headers";



@Injectable()
export class RestService {

  constructor(public http: Http) {

  }
  get(url:string):Observable<any> {
    this.startLoader();
    this.setHeader();
    let options = new RequestOptions({
      body:{},
      headers: contentHeaders,
    });
    return this.http.get(url,options).map(
      (response) =>this.sendResponse(response)
    ).catch(this.handleErrorObservable);
  }

  getPromise(url:string):Promise <any> {

    let options = new RequestOptions({
      body:{},
      headers: contentHeaders,
    });
    return this.http.get(url,options).toPromise().then(
      (response) => {
        return this.sendResponse(response);

      }
    ).catch(this.handleErrorObservable);
  }

  getGoogle(url:string):Observable<any> {

    this.startLoader();
    this.setHeader();
    let options = new RequestOptions({
      body:{},
      // headers: googleHeaders,
    });
    return this.http.get(url,options).map(
      (response) =>this.sendResponse(response)
    ).catch(this.handleErrorObservable);
  }



  post(url:string, data?):Observable<any>  {
    if (data=== undefined){
      data ={}
    }
    this.startLoader();
    return this.http.post(url, data, { headers: contentHeaders }).map(
      (response) =>{
        return this.sendResponse(response)
      }
    ).catch(this.handleErrorObservable);
  }

  // postFile(url:string, data?):Observable<any>  {
  //   if (data=== undefined){
  //     data ={}
  //   }
  //   this.startLoader();
  //   return this.http.post(url, data, { headers: fileHeaders }).map(
  //     (response) =>{
  //       return this.sendResponse(response)
  //     }
  //   ).catch(this.handleErrorObservable);
  // }

  postPromise(url : string, data) : Promise<any> {
    if (data=== undefined){
      data ={}
    }
    return this.http.post(url, data, { headers: contentHeaders }).toPromise()
      .then(response => {
        return this.sendResponse(response)
      }).catch(this.handleErrorObservable);

  }


  patch(url:string, object):Observable<any>{
    this.startLoader();
    this.setHeader();
    let obj = JSON.stringify(object);
    return this.http.patch(url, obj,  { headers: contentHeaders })
      .map(
        (response) => this.sendResponse(response)
      ).catch(this.handleErrorObservable);
  }

  put(url:string, object):Observable<any>{
    this.startLoader();
    this.setHeader();
    let obj = JSON.stringify(object);
    return this.http.put(url, obj,  { headers: contentHeaders })
      .map(
        (response) => this.sendResponse(response)
      ).catch(this.handleErrorObservable);
  }

  del(url: string) : Promise<any> {

    return this.http.delete(url, {headers : contentHeaders}).toPromise()
      .then((response) => response)
      .catch(this.handleErrorObservable);
  }


  getCookie(name: string) {
    // let value = "; " + document.cookie;
    // let parts = value.split("; " + name + "=");
    // if (parts.length == 2)
    //   return parts.pop().split(";").shift();

    let ca: Array<string> = document.cookie.split(';');
    let caLen: number = ca.length;
    let cookieName = name + "=";
    let c: string;

    for (let i: number = 0; i < caLen; i += 1) {
      c = ca[i].replace(/^\s\+/g, "");
      if (c.indexOf(cookieName) == 0) {
        return c.substring(cookieName.length, c.length);
      }
    }
    return "";

  }

  setHeader(){
  }

  startLoader(){
  }

  sendResponse(response){
    return response.json();
  }

  private handleErrorObservable (error: any) {
    EmitterService.get("loading").emit(false);
    // In a real world app, we might send the error to remote logging infrastructure
    let errMsg = JSON.parse(error._body).error_code || '500';
    /******** to check the minimum amount error only for error code 907 ****/
    if(errMsg == "907"){
      let err_obj = JSON.parse(error._body);
      return Observable.throw(err_obj);
    }
    /*********************************************/
    return Observable.throw(errMsg);
  }

}
