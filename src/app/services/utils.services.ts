/**
 * Created by consultadd on 6/12/16.
 */
import {Injectable, EventEmitter} from '@angular/core';
import 'rxjs/Rx';


@Injectable()
export class EmitterService {
  private static _emitters: { [channel: string]: EventEmitter<any> } = {};
  static get(channel: string): EventEmitter<any> {
    if (!this._emitters[channel])
      this._emitters[channel] = new EventEmitter();
    return this._emitters[channel];
  }

  static remove(channel: string){
    if(this._emitters[channel])
      this._emitters[channel] = null;
  }
}
