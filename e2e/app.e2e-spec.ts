import { TixdoAdminPage } from './app.po';

describe('tixdo-admin App', function() {
  let page: TixdoAdminPage;

  beforeEach(() => {
    page = new TixdoAdminPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
